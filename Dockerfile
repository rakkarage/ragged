FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
WORKDIR /app
EXPOSE 80

COPY *.sln .
COPY Gename/*.csproj ./Gename/
COPY Ragged/*.csproj ./Ragged/
RUN dotnet restore

COPY Ragged/. ./Ragged/
COPY Gename/. ./Gename/
WORKDIR /app/Ragged
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2 AS runtime
WORKDIR /app
COPY --from=build /app/Ragged/out ./
ENTRYPOINT ["dotnet", "Ragged.dll"]
